﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IdentityServiceApi.Models
{
    public class ApplicationUser : IdentityUser
    {
        public virtual bool ChangePasswordRequired { get; set; }
        public virtual string Firstname { get; set; }
        public virtual string Lastname { get; set; }
        public virtual DateTime? InvitedOn { get; set; }
        public virtual bool IsEnabled { get; set; }
    }
}