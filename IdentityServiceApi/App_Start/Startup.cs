﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using System.Configuration;
using IdentityServer3.AccessTokenValidation;

[assembly: OwinStartup(typeof(IdentityServiceApi.App_Start.Startup))]

namespace IdentityServiceApi.App_Start
{
    public class Startup
    {
        private readonly string IdServBaseUri = @ConfigurationManager.AppSettings["identityserveraddress"];

        public void Configuration(IAppBuilder app)
        {
            app.CreatePerOwinContext(IdentityServiceContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);

            app.Map(
               "/api",
               inner =>
               {
                   inner.UseIdentityServerBearerTokenAuthentication(new IdentityServerBearerTokenAuthenticationOptions
                   {
                       Authority = IdServBaseUri,
                       RequiredScopes = new[] { "belronidentityserviceapi" },
                       NameClaimType = "name",
                       RoleClaimType = "role"
                   });
               });
        }
    }
}
