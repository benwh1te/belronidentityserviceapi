﻿using System.Configuration;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Cors;

namespace IdentityServiceApi
{
    public static class WebApiConfig
    {
        private static string GetAllowedOrigins()
        {
            return ConfigurationManager.AppSettings["AllowedCrossOrigins"];
        }

        public static void Register(HttpConfiguration config)
        {
            //string origins = GetAllowedOrigins();
            //var cors = new EnableCorsAttribute(origins, "*", "*");

            //Enable CORS as we'll be calling this from the market place.
            //config.EnableCors(cors);

            //Allow api to return JSON
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
