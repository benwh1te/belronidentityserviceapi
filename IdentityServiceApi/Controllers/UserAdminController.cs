﻿using IdentityServiceApi.App_Start;
using IdentityServiceApi.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Linq;

namespace IdentityServiceApi.Controllers
{
    [Authorize]
    [RoutePrefix("api/UserAdmin")]
    public class UserAdminController : ApiController
    {
        
        public bool CreateNewIdentityUser([FromBody]ApplicationUser user)
        {
            try
            {
                ApplicationUserManager userManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
                bool userCreated = false;

                var applicationUser = new ApplicationUser();
                applicationUser.Email = user.Email;
                applicationUser.UserName = user.UserName;
                applicationUser.ChangePasswordRequired = false;
                applicationUser.Firstname = user.Firstname;
                applicationUser.Lastname = user.Lastname;
                applicationUser.Id = user.Id;
                applicationUser.PasswordHash = user.PasswordHash;
                applicationUser.SecurityStamp = user.SecurityStamp;
                applicationUser.EmailConfirmed = true;
                applicationUser.InvitedOn = user.InvitedOn;
                applicationUser.IsEnabled = true;

                userManager.Create(applicationUser);

                if(userManager.FindById(applicationUser.Id)!=null)
                {
                    userCreated = true;
                }
                return userCreated;
            }
            catch(Exception ex)
            {
                throw new Exception("Unable to create Identity User via the Identity Service API");
            }
        }

        public bool RemoveIdentityUser([FromBody]ApplicationUser user)
        {
            try
            {
                ApplicationUserManager userManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
                bool userDeleted = true;

                var _user = userManager.FindById(user.Id);
                    
                if(_user!=null)
                {
                    userManager.Delete(_user);
                }

                return false;
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to delete Identity User via the Identity Service API");
            }
        }

        public bool UpdateIdentityUserPassword([FromBody]ApplicationUser user)
        {
            try
            {
                IdentityServiceContext dbContext = HttpContext.Current.GetOwinContext().Get<IdentityServiceContext>();
                using (dbContext)
                {
                    var identityUser = dbContext.Users.Where(u => u.Id == user.Id).First();
                    if (identityUser != null)
                    {
                        identityUser.PasswordHash = user.PasswordHash;
                        identityUser.SecurityStamp = user.SecurityStamp;

                        dbContext.SaveChanges();
                        return true;
                    }
                }
            }
            catch(Exception ex)
            {
                throw new Exception("Unable to Update Identity User Password via the Identity Service API");
            }
            return false;
        }

        public bool UpdateIdentityPassword([FromBody]ApplicationUser user)
        {
            try
            {
                IdentityServiceContext dbContext = HttpContext.Current.GetOwinContext().Get<IdentityServiceContext>();
                using (dbContext)
                {
                    var identityUser = dbContext.Users.Where(u => u.Id == user.Id && u.Email == user.Email).First();
                    if (identityUser != null)
                    {
                        identityUser.PasswordHash = user.PasswordHash;
                        identityUser.SecurityStamp = user.SecurityStamp;

                        dbContext.SaveChanges();
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to Update Identity User Password via the Identity Service API");
            }
            return false;
        }

        public bool UpdateIdentityUserProfileData([FromBody]ApplicationUser user)
        {
            try
            {
                IdentityServiceContext dbContext = HttpContext.Current.GetOwinContext().Get<IdentityServiceContext>();
                using (dbContext)
                {
                    var identityUser = dbContext.Users.Where(u => u.Id == user.Id).First();
                    if (identityUser != null)
                    {
                        identityUser.Firstname = user.Firstname;
                        identityUser.Lastname = user.Lastname;
                        dbContext.SaveChanges();
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to Update Identity User Password via the Identity Service API");
            }
            return false;
        }
    }
}