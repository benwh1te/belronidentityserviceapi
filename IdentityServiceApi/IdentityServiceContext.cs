﻿using IdentityServiceApi.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IdentityServiceApi
{
    public class IdentityServiceContext : IdentityDbContext<ApplicationUser>
    {
        public IdentityServiceContext() : base("name=IdentityServiceContext")
        {
        }

        public static IdentityServiceContext Create()
        {
            return new IdentityServiceContext();
        }
    }
}